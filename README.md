# Genders of Countries

When you have learned country names in a language, without knowing their grammatical genders, you can't actually compose sentences.
This project aims to solve this problem.